﻿using System;

namespace exercise18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var num1 = 1;
            var num2 = 2;
            var num3 = 3;
            var num4 = 4;
            var num5 = 5;
            Console.WriteLine($"The value of number 1 = {num1}");
            Console.WriteLine($"The value of number 2 = {num2}");
            Console.WriteLine($"The value of number 3 = {num3}");
            Console.WriteLine($"The value of number 4 = {num4}");
            Console.WriteLine($"The value of number 5 = {num5}");

            var num6 = 6;
            var num7 = 7;
            var num8 = 8;
            var num9 = 9;
            var num10 = 10;
            var num11 = 11;
            Console.WriteLine($"num1 + num6 = {num1+num6}");
            Console.WriteLine($"num2 + num7 = {num2+num7}");
            Console.WriteLine($"num3 + num8 = {num3+num8}");
            

            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
